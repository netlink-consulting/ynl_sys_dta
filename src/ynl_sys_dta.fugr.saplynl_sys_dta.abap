*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LYNL_SYS_DTATOP.                   " Global Declarations
  INCLUDE LYNL_SYS_DTAUXX.                   " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LYNL_SYS_DTAF...                   " Subroutines
* INCLUDE LYNL_SYS_DTAO...                   " PBO-Modules
* INCLUDE LYNL_SYS_DTAI...                   " PAI-Modules
* INCLUDE LYNL_SYS_DTAE...                   " Events
* INCLUDE LYNL_SYS_DTAP...                   " Local class implement.
INCLUDE LYNL_SYS_DTAP00.                   " Local class implement. for Exeptions
INCLUDE LYNL_SYS_DTAP0A.                   " Local class implement. for Authorizations
INCLUDE LYNL_SYS_DTAP10.                   " Local class implement.
* INCLUDE LYNL_SYS_DTAT99.                   " ABAP Unit tests
