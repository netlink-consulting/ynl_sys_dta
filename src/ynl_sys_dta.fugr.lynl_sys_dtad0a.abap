CLASS lcl_authorization_check DEFINITION CREATE PRIVATE.

  PUBLIC SECTION.

    CLASS-METHODS:
      view_table IMPORTING !table_name TYPE tabname
                 RAISING   lcx_not_authorized
                           lcx_table_not_found
                           lcx_table.
ENDCLASS.
