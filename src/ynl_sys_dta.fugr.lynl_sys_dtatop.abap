FUNCTION-POOL ynl_sys_dta.                  "MESSAGE-ID ..

INCLUDE lynl_sys_dtad00.                   " Local class definition for Exceptions
INCLUDE lynl_sys_dtad0a.                   " Local class definition for Authorizations
INCLUDE lynl_sys_dtad10.                   " Local class definition

DATA: rfc_get_tbl_dta_table TYPE REF TO lcl_table.  " Persistent to allow batches
