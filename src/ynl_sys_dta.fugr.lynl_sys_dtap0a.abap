CLASS lcl_authorization_check IMPLEMENTATION.

  METHOD view_table.

    CALL FUNCTION 'VIEW_AUTHORITY_CHECK'
      EXPORTING
        view_action                    = 'S'              " Action: Display/Change/Transport (S/U/T)
        view_name                      = table_name       " Name of the table/view to be checked
        no_warning_for_clientindep     = abap_true        " Flag: No Warning for Cross-Client Objects
      EXCEPTIONS
        invalid_action                 = 1                " The specified action is not allowed
        no_authority                   = 2                " The specified table/view action is not allowed
        no_clientindependent_authority = 3                " No Authorization for Maintaining Cross-Client Tables/Views
        table_not_found                = 4                " The specified view/table does not exist
        no_linedependent_authority     = 5                " No Row Authorization
        OTHERS                         = 6.
    CASE sy-subrc.
      WHEN 1 OR 6.
        RAISE EXCEPTION TYPE lcx_table EXPORTING table_name = table_name.
      WHEN 2 OR 3 OR 5.
        RAISE EXCEPTION TYPE lcx_not_authorized EXPORTING table_name = table_name.
      WHEN 4.
        RAISE EXCEPTION TYPE lcx_table_not_found EXPORTING table_name = table_name.
    ENDCASE.

  ENDMETHOD.

ENDCLASS.
