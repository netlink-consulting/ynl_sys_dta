CLASS lcl_table_struct IMPLEMENTATION.

  METHOD constructor.

    TYPES: BEGIN OF field_row,
             fieldname TYPE fieldname,
             leng      TYPE ddleng,
             decimals  TYPE decimals,
             datatype  TYPE dynptype,
             inttype   TYPE inttype,
             fieldtext TYPE as4text,
             keyflag   TYPE keyflag,
             lowercase TYPE lowercase,
           END OF field_row.

    DATA: components       TYPE HASHED TABLE OF cl_abap_structdescr=>component WITH UNIQUE KEY name,
          field_list       TYPE ddfields,
          field_list_temp  TYPE STANDARD TABLE OF field_row,
          field_table      TYPE STANDARD TABLE OF field_row,
          header           TYPE x030l,
          structdescr      TYPE REF TO cl_abap_structdescr,
          table_components TYPE cl_abap_structdescr=>component_table.


    structdescr ?=  cl_abap_typedescr=>describe_by_name( p_name = table_name ).

    " check if table_name is a database table
    header = structdescr->get_ddic_header( ).
    IF header-tabtype <> 'T'.
      RAISE EXCEPTION TYPE lcx_not_data_table EXPORTING table_name = table_name.
    ENDIF.

    " if not fields provided, use all
    field_list = structdescr->get_ddic_field_list( ).
    MOVE-CORRESPONDING field_list TO field_list_temp.
    IF lines( fields ) = 0.
      LOOP AT field_list ASSIGNING FIELD-SYMBOL(<field_list>).
        APPEND <field_list>-fieldname TO me->fields.
      ENDLOOP.
    ELSE.
      me->fields = fields.
    ENDIF.

    " using the fields, build a dynamic structure using the description from DDIC and data to retrun
    components = structdescr->get_components( ).

    LOOP AT me->fields ASSIGNING FIELD-SYMBOL(<field>).
      TRY.
          APPEND components[ name = <field> ] TO table_components.
          APPEND field_list_temp[ fieldname = <field> ] TO field_table.
        CATCH cx_sy_itab_line_not_found.
          RAISE EXCEPTION TYPE lcx_column_not_in_table EXPORTING table_name = table_name column_name = <field>.
      ENDTRY.
      CONCATENATE me->field_selector ',' <field> INTO me->field_selector.

    ENDLOOP.
    me->field_selector = substring( val = me->field_selector off = 1 ).

    DATA(o_writer_itab_xml) = cl_sxml_string_writer=>create( type = if_sxml=>co_xt_xml10 ).
    CALL TRANSFORMATION id SOURCE values = field_table RESULT XML o_writer_itab_xml.
    me->struct_xml = cl_abap_codepage=>convert_from( o_writer_itab_xml->get_output( ) ).

    me->row_descr  ?= cl_abap_structdescr=>create( p_components = table_components ).
    me->table_descr = cl_abap_tabledescr=>create( p_line_type = me->row_descr ).

  ENDMETHOD.

ENDCLASS.



CLASS lcl_table IMPLEMENTATION.

  METHOD constructor.

    me->table_name = table_name.
    me->fields = fields.
    me->where = where.
    IF batchsize > 0.
      me->batchsize = batchsize.
    ELSE.
      me->batchsize =  2147483646.
    ENDIF.
    me->maxrows = maxrows.

    me->table_struct = NEW lcl_table_struct( table_name = me->table_name
                                             fields     = me->fields ).

    load_table(  ).

    cursor = 0.

  ENDMETHOD.

  METHOD load_table.

    DATA: rows TYPE i VALUE 2147483646.

    CREATE DATA: me->data_table TYPE HANDLE me->table_struct->table_descr.

    FIELD-SYMBOLS: <dynamic_table> TYPE STANDARD TABLE.

    ASSIGN me->data_table->* TO <dynamic_table>.

    IF me->maxrows > 0.
      rows = me->maxrows.
    ENDIF.

    SELECT (me->table_struct->field_selector) FROM (me->table_name) WHERE (me->where)
    INTO TABLE @<dynamic_table> UP TO @rows ROWS.


  ENDMETHOD.

  METHOD get_next_batch.

    DATA: batch      TYPE i,
          temp_table TYPE REF TO data.

    IF batchsize > 0.
      batch = batchsize.
    ELSE.
      batch = me->batchsize.
    ENDIF.

    FIELD-SYMBOLS: <dynamic_table> TYPE STANDARD TABLE,
                   <temp_table>    TYPE STANDARD TABLE..

    CREATE DATA: temp_table TYPE HANDLE me->table_struct->table_descr.

    ASSIGN me->data_table->* TO <dynamic_table>.
    ASSIGN temp_table->* TO <temp_table>.

    APPEND LINES OF <dynamic_table> FROM me->cursor + 1 TO me->cursor + batch TO <temp_table>.
    ADD batch TO me->cursor.

    DATA(o_writer_itab_xml) = cl_sxml_string_writer=>create( type = if_sxml=>co_xt_xml10 ).
    CALL TRANSFORMATION id SOURCE values = <temp_table> RESULT XML o_writer_itab_xml.
    result = cl_abap_codepage=>convert_from( o_writer_itab_xml->get_output( ) ).


  ENDMETHOD.

  METHOD get_result_struct.

    result = me->table_struct->struct_xml.

  ENDMETHOD.

  METHOD is_same.

    IF    me->table_name = table_name
      AND me->fields = fields
      AND me->where = where
      AND me->maxrows = maxrows.
      result = abap_true.
    ELSE.
      result = abap_false.
    ENDIF.

  ENDMETHOD.

ENDCLASS.
