FUNCTION YNL_SYS_DTA_RFC_GET_TBL_DTA.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(TABLE_NAME) TYPE  TABNAME
*"     VALUE(FIELDS) TYPE  YNL_SYS_DTA_RFC_GET_TBL_DTA_FT OPTIONAL
*"     VALUE(WHERE) TYPE  STRING OPTIONAL
*"     VALUE(BATCHSIZE) TYPE  I DEFAULT 0
*"     VALUE(MAXROWS) TYPE  I DEFAULT 0
*"     VALUE(CLEAR) TYPE  CHAR01 DEFAULT SPACE
*"  EXPORTING
*"     VALUE(RESULT) TYPE  STRING
*"     VALUE(STRUCT) TYPE  STRING
*"     VALUE(RETURN) TYPE  BAPIRET2
*"----------------------------------------------------------------------

  FREE: return.

  TRY.
      lcl_authorization_check=>view_table( table_name = table_name ).
    CATCH lcx_not_authorized.
      return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA' number = '101' par1 = CONV #( table_name ) ).
      RETURN.
    CATCH lcx_table_not_found.
      return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA' number = '102' par1 = CONV #( table_name ) ).
      RETURN.
    CATCH lcx_table.
      return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA' number = '000' par1 = 'Unknown error.' ).
      RETURN.
  ENDTRY.

  IF rfc_get_tbl_dta_table IS BOUND.
    IF  rfc_get_tbl_dta_table->is_same(
          table_name = table_name
          fields     = fields
          where      = where
          maxrows    = maxrows
        ) <> abap_true.
      clear = abap_true.
    ENDIF.
  ENDIF.

  IF rfc_get_tbl_dta_table IS NOT BOUND OR clear = abap_true.
    TRY.
        rfc_get_tbl_dta_table = NEW lcl_table( table_name = table_name
                                               fields     = fields
                                               where      = where
                                               batchsize  = batchsize
                                               maxrows    = maxrows ).
      CATCH lcx_table_not_found.
        return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA' number = '102' par1 = CONV #( table_name ) ).
        RETURN.
      CATCH lcx_not_data_table.
        return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA' number = '103' par1 = CONV #( table_name ) ).
        RETURN.
      CATCH lcx_column_not_in_table INTO DATA(exc).
        return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA'  number = '104'  par1 = CONV #( table_name )  par2 = CONV #( exc->column_name ) ).
        RETURN.
      CATCH lcx_table.
        return = ynl_share=>bapiret2( type = 'E'  cl = 'YNL_SYS_DTA' number = '000' par1 = 'Unknown error.' ).
        RETURN.

    ENDTRY.
    struct = rfc_get_tbl_dta_table->get_result_struct( ).
  ENDIF.

  result = rfc_get_tbl_dta_table->get_next_batch( batchsize ).

ENDFUNCTION.
