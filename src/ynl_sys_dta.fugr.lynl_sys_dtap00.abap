CLASS lcx_table IMPLEMENTATION.

  METHOD constructor.

    super->constructor( previous = previous ).
    CLEAR me->textid.
    IF textid IS INITIAL.
      if_t100_message~t100key = if_t100_message=>default_textid.
    ELSE.
      if_t100_message~t100key = textid.
    ENDIF.
    me->table_name = table_name.

  ENDMETHOD.

ENDCLASS.


CLASS lcx_column_not_in_table IMPLEMENTATION.

  METHOD constructor.

    super->constructor( table_name = table_name  previous = previous ).
    CLEAR me->textid.
    IF textid IS INITIAL.
      if_t100_message~t100key = VALUE #( msgid = 'YNL_SYS_DTA'
                                         msgno = '104'
                                         attr1 = 'TABLE_NAME'
                                         attr2 = 'COLUMN_NAME'
                                         attr3 = ''
                                         attr4 = '' ).
    ELSE.
      if_t100_message~t100key = textid.
    ENDIF.
    me->column_name = column_name.

  ENDMETHOD.

ENDCLASS.
