CLASS lcl_table_struct DEFINITION CREATE PUBLIC.

  PUBLIC SECTION.

    DATA: fields         TYPE ynl_sys_dta_rfc_get_tbl_dta_ft READ-ONLY,
          field_selector TYPE string READ-ONLY,
          struct_xml     TYPE string READ-ONLY,
          row_descr      TYPE  REF TO  cl_abap_datadescr READ-ONLY,
          table_descr    TYPE REF TO  cl_abap_tabledescr READ-ONLY.

    METHODS:
      constructor        IMPORTING !table_name TYPE tabname
                                   !fields     TYPE ynl_sys_dta_rfc_get_tbl_dta_ft
                         RAISING   lcx_not_data_table
                                   lcx_column_not_in_table.

ENDCLASS.


CLASS lcl_table DEFINITION CREATE PUBLIC.

  PUBLIC SECTION.

    METHODS:
      constructor       IMPORTING table_name TYPE tabname
                                  fields     TYPE ynl_sys_dta_rfc_get_tbl_dta_ft
                                  where      TYPE string
                                  batchsize  TYPE i
                                  maxrows    TYPE i
                        RAISING   lcx_table_not_found
                                  lcx_not_data_table
                                  lcx_column_not_in_table
                                  lcx_table,

      get_next_batch    IMPORTING batchsize     TYPE i DEFAULT 0
                        RETURNING VALUE(result) TYPE string,

      get_result_struct RETURNING VALUE(result) TYPE string,

      is_same           IMPORTING table_name    TYPE tabname
                                  fields        TYPE ynl_sys_dta_rfc_get_tbl_dta_ft
                                  where         TYPE string
                                  maxrows       TYPE i
                        RETURNING VALUE(result) TYPE abap_bool.

  PROTECTED SECTION.

    DATA: table_name   TYPE tabname,
          fields       TYPE ynl_sys_dta_rfc_get_tbl_dta_ft,
          where        TYPE string,
          batchsize    TYPE i,
          maxrows      TYPE i,

          table_struct TYPE REF TO lcl_table_struct,

          data_table   TYPE REF TO data,

          cursor       TYPE i.

    METHODS:
      load_table.

  PRIVATE SECTION.

ENDCLASS.
