CLASS lcx_root DEFINITION INHERITING FROM cx_dynamic_check CREATE PUBLIC.
ENDCLASS.


CLASS lcx_table DEFINITION INHERITING FROM lcx_root CREATE PUBLIC.
  PUBLIC SECTION.
    INTERFACES:
      if_t100_message.
    DATA:
      table_name TYPE tabname.
    METHODS:
      constructor IMPORTING table_name TYPE tabname
                            textid     LIKE if_t100_message=>t100key OPTIONAL
                            previous   LIKE previous OPTIONAL.
ENDCLASS.

CLASS lcx_not_authorized DEFINITION INHERITING FROM lcx_table.
ENDCLASS.


CLASS lcx_table_not_found DEFINITION INHERITING FROM lcx_table.
ENDCLASS.


CLASS lcx_not_data_table DEFINITION INHERITING FROM lcx_table.
ENDCLASS.


CLASS lcx_column_not_in_table DEFINITION INHERITING FROM lcx_table.
  PUBLIC SECTION.
    DATA:
      column_name TYPE fieldname.
    METHODS:
      constructor IMPORTING table_name  TYPE tabname
                            column_name TYPE fieldname
                            textid      LIKE if_t100_message=>t100key OPTIONAL
                            previous    LIKE previous OPTIONAL.
ENDCLASS.
